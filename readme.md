# Projeto Webjump

Reconheço muitos pontos de melhoria no material que estou entregando, como melhorar a experiência do usuario validação de dados, criar classes para executar as funções de banco de dados...

Meus commits são bem mais organizados no ambiente de produção, separando os arquivos e commitando sempre que criado ou alterado algum recurso

O recurso de importação de csv confesso que nunca programei nada parecido, embora tenha elaborado a lógica para fazê-lo o tempo livre que tive para me dedicar ao projeto influenciou nas decisões tomadas

Contudo por estar em trablhando em horário comercial e ter assumido previamente outros compromissos no decorrer da semana tive o tempo reduzido para me dedicar a esse projeto

Segue observações e instruções de configuração

## Observações

-   CRUD sendo realizado via AJAX com upload de imagem
-   O INSERT e UPDATE do produto estão utilizando Jquery, mas sempre dou preferencia para javascript puro...
-   Ficou faltando o recurso de importação de csv

## Configurar projeto

-   importar arquivo database.sql na raiz do projeto para banco de dados
-   configurar conexao com o banco no arquivo connect.php localizdo na pasta inc/
-   configurar link base para página na linha 07 do arquivo index.php localizado na raiz do projeto
