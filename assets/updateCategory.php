<?php 
    extract($_GET);
    require 'inc/connect.php';

    $con = mysqli_connect($host, $user, $pass, $base);
    $sql="select category from tbl_categories where id = $id limit 1";
    $query = mysqli_query($con, $sql);

    foreach( $query as $row ){
        extract($row);
    }
?>


  <main class="content">
    <h1 class="title new-item">Editar</h1>
    
    <form onsubmit="addCategory(this); return false;">

      <div class="input-field">
        <label for="category" class="label">Categoria</label>
        <input type="text" id="category" class="input-text" value="<?= $category; ?>" />
      </div>
      
      <div class="actions-form">
        <a href="categoria" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="button" value="Save" onclick="update_category(<?= $id; ?>)"/>
      </div>
    </form>
  </main>



  <script>

    update_category = function(id) {
      
      category = document.querySelector('#category').value;
      submit   = document.querySelector('.btn-submit');
          
      data = `action=updateCategory&category=${category}&id=${id}`;

      submit.setAttribute('disabled', 'true');
      submit.innerHTML = 'Enviando...';

      request = new XMLHttpRequest;
      request.open('POST', 'inc/update.php', true);
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      request.send(data);

      request.onload = function() {

          response = request.responseText;

          if (this.status >= 200 && this.status < 400) {


              if (response == 'ok') {

                  window.location = 'categorias';

              } else {
                  
                  alert(response);
                  submit.removeAttribute('disabled');
                  submit.value = 'ENVIAR';

              }

          } else {

              alert(response);
              submit.removeAttribute('disabled');
              submit.value = 'ENVIAR';

          }

      }

    }
  </script>